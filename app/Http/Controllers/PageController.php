<?php

namespace App\Http\Controllers;

use App\Http\Requests\SavePostRequest;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PageController extends Controller
{
    public function home(){
        $posts = Post::all('id','title','content','created_at');
        return view('home',['posts'=>$posts]);
    }
    public function create(){
        return view('posts.create');
    }
    public function index(){
        return "ok";
    }
    public function save(SavePostRequest $request){
        // Sauvegarde en passant par une instance du model
        // $post = new Post();
        // $post->title = $request->title;
        // $post->content = $request->content;
        // $post->save();

        // Mass assignment



        Post::create($request->all());
        return redirect()->route('home');


    }
}
