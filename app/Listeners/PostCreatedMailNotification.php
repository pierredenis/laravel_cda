<?php

namespace App\Listeners;

use App\Mail\PostCreated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class PostCreatedMailNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {

        \App\Jobs\PostCreatedMailNotification::dispatch($event->post);
        //Mail::to('admin@admin.com')->send(new PostCreated($event->post));
    }
}
