<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Post extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function tags(){
        return $this->belongsToMany(Tag::class);
    }

    /**
     * Get the excerpt's post content.
     *
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    protected function excerpt(): Attribute
    {
        return Attribute::make(
            get: fn ($value,$attributes) => substr($attributes['content'],0,2),
        );
    }

    /**
     * Set slug's post
     *
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    protected function title(): Attribute
    {
        return Attribute::make(
            get: fn($value) => Str::upper($value),
            set: fn ($value) => [
                'title' => $value,
                'slug' => Str::slug($value)
            ]
        );
    }
}
